package com.ch.iot.framework;

import com.ch.iot.arm.Light;

/**
 * Created by courtney harleston
 * 10/22/15
 */
public class SystemManager {
    private Light successLight;
    private Light failLight;
    private SystemManager(Context context) {
        successLight = Light.open(Integer.parseInt(context.getProperty("successLight.pin")));
        failLight = Light.open(Integer.parseInt(context.getProperty("failLight.pin")));
    }

    public void success() throws Exception {
        successLight.on();
        failLight.off();
    }

    public void failure() throws Exception {
        failLight.off();
        successLight.off();
    }

    static SystemManager createInstance(Context context) {
        return SystemManagerHelper.INSTANCE(context);
    }

    public void off() throws Exception {
        successLight.release();
        failLight.release();
    }

    private static class SystemManagerHelper{
        private static SystemManager INSTANCE(Context context){
            return new SystemManager(context);
        }
    }
}
