package com.ch.iot.framework;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * Created by courtney harleston
 * 10/22/15
 */
public class StatusManager {
    private static Logger LOGGER = Logger.getLogger(StatusManager.class.getName());
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final String url;
    private volatile boolean running = true;
    private final String deviceId;
    private StatusChange statusChange;
    private volatile int status = 0;

    private StatusManager(Context context) {
        deviceId = context.getProperty("deviceId");
        url = context.getProperty("status.url");
        checkStatus();
    }

    static StatusManager createInstance(Context context){
        return StatusManagerHelper.INSTANCE(context);
    }

    private void checkStatus() {
        EXECUTOR_SERVICE.execute(() -> {
            while (running) {
                try {
                    if((status != (status = currentStatus())) && (statusChange != null)){
                        statusChange.setStatus(status);
                    }
                } catch (Exception e) {
                    LOGGER.severe(e.getMessage());
                }
                sleep();
            }
        });
    }

    private void sleep() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
           running = false;
        }
    }

    private synchronized int currentStatus() throws Exception {
        URL u = new URL(url);
        try (InputStream inputStream = u.openStream();JsonReader jsonReader = Json.createReader(inputStream)) {
            JsonArray jsonArray = jsonReader.readArray();
            for (JsonObject result : jsonArray.getValuesAs(JsonObject.class)) {
                if (deviceId.equals(String.valueOf(result.get("deviceId")))){
                    return result.getBoolean("isOccupied") ? 1 : 0;
                }
            }
        }
        return status;
    }

    public synchronized void updateStatus() throws Exception{
        try (CloseableHttpClient closeableHttpClient = HttpClientBuilder.create().build()) {
            HttpResponse response =
                    closeableHttpClient.execute(
                            new HttpGet(String.format("%s?device=%s&occupied=%s", url, deviceId, currentStatus() ^ 1)));
            int statusCode = response.getStatusLine().getStatusCode();
            if (200 != statusCode){
                throw new HttpResponseException(statusCode, "status code should be 200");
            }
        }
    }

    public void registerStatusChange(StatusChange statusChange) {
        this.statusChange = statusChange;
    }

    private static class StatusManagerHelper {
        private static StatusManager INSTANCE(Context context){
            return new StatusManager(context);
        }
    }

    public interface StatusChange {
        void setStatus(int status);
    }
}
