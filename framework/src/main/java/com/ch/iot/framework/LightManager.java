package com.ch.iot.framework;

import com.ch.iot.arm.Light;

/**
 * Created by courtney harleston
 * 10/4/15
 */
public class LightManager {
    private Light light;

    private LightManager(Context context) {
        light = Light.open(Integer.parseInt(context.getProperty("light.pin")));
    }

    static LightManager createInstance(Context context) {
        return LightManagerHelper.INSTANCE(context);
    }

    private static class LightManagerHelper {
        public static LightManager INSTANCE(Context context) {
            return new LightManager(context);
        }
    }

    public void on() throws Exception {
        light.on();
    }

    public void off() throws Exception {
        light.off();
    }
}
