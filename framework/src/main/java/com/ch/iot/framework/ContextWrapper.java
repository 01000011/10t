package com.ch.iot.framework;

/**
 * Created by courtney harleston
 * 10/3/15
 */
public class ContextWrapper extends Context {
    private Context baseContext;

    public ContextWrapper(){
        this(null);
    }

    public ContextWrapper(Context context) {
        setBaseContext(context);
    }

    @Override
    public Context getApplicationContext() {
        return baseContext.getApplicationContext();
    }

    @Override
    public Object getSystemService(String name) {
        return baseContext.getSystemService(name);
    }

    @Override
    public String getProperty(String name) {
        return baseContext.getProperty(name);
    }

    protected final void setBaseContext(Context context){
        this.baseContext = context;
    }
}
