package com.ch.iot.framework;

import com.ch.iot.arm.HCSR501MotionSensor;

import java.util.logging.Logger;

/**
 * Created by courtney harleston
 * 10/23/15
 */
public class MotionManager implements HCSR501MotionSensor.SensorAction {
    private static final Logger LOGGER = Logger.getLogger(MotionManager.class.getName());
    private Context context;

    private MotionManager(Context context) {
        this.context = context;
        HCSR501MotionSensor hcsr501MotionSensor =
                HCSR501MotionSensor.read(Integer.parseInt(context.getProperty("hcsr501MotionSensor.pin")), this);
    }

    static MotionManager createInstance(Context context) {
        return MotionManagerHelper.INSTANCE(context);
    }

    @Override
    public void onMotionDetected() {
        LOGGER.info("onMotionDetected");
    }

    @Override
    public long getMotionPollingInterval() {
        return 1;
    }

    @Override
    public void setMotionError(String e) {

    }

    @Override
    public void onMotionResetNormal() {

    }

    private static class MotionManagerHelper{
        private static MotionManager INSTANCE(Context context){
            return new MotionManager(context);
        }
    }
}
