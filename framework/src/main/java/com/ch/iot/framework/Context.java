package com.ch.iot.framework;

/**
 * Created by courtney harleston
 * 10/3/15
 */
public abstract class Context {
    public static final String STATUS_SERVICE = "STATUS_SERVICE";
    public static final String SYSTEM_SERVICE = "SYSTEM_SERVICE";
    public static final String MOTION_SERVICE = "MOTION_SERVICE";
    public static final String LIGHT_SERVICE = "LIGHT_SERVICE";

    public abstract Context getApplicationContext();
    public abstract Object getSystemService(String name);
    public abstract String getProperty(String name);
}
