package com.ch.iot.framework;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by courtney harleston
 * 10/3/15
 */
public class ApplicationContext extends Context {
    private static Logger LOGGER = Logger.getLogger(ApplicationContext.class.getName());
    private static final Map<String, SystemService> SYSTEM_SERVICES = Collections.synchronizedMap(new HashMap<>());

    private Properties properties = new Properties();
    static {
        logRegisteringService(STATUS_SERVICE);
        registerSystemService(STATUS_SERVICE, new SystemService() {
            @Override
            public Object createService(ApplicationContext applicationContext) {
                return StatusManager.createInstance(applicationContext);
            }
        });
        logRegisteringService(SYSTEM_SERVICE);
        registerSystemService(SYSTEM_SERVICE, new SystemService() {
            @Override
            public Object createService(ApplicationContext applicationContext) {
                return SystemManager.createInstance(applicationContext);
            }
        });
        logRegisteringService(MOTION_SERVICE);
        registerSystemService(MOTION_SERVICE, new SystemService() {
            @Override
            public Object createService(ApplicationContext applicationContext) {
                return MotionManager.createInstance(applicationContext);
            }
        });
        logRegisteringService(LIGHT_SERVICE);
        registerSystemService(LIGHT_SERVICE, new SystemService() {
            @Override
            public Object createService(ApplicationContext applicationContext) {
                return LightManager.createInstance(applicationContext);
            }
        });
    }

    private static void logRegisteringService(String statusService) {
        LOGGER.info(String.format("registering service [%s]", statusService));
    }

    private static void registerSystemService(String name, SystemService systemService) {
        SYSTEM_SERVICES.put(name, systemService);
    }

    public ApplicationContext() throws Exception {
        initialize();
    }

    private void initialize() throws IOException {
        properties.load(ApplicationContext.class.getResourceAsStream("/system.properties"));
    }

    @Override
    public Context getApplicationContext() {
        return this;
    }

    @Override
    public Object getSystemService(String name) {
        return (name == null) ? null : SYSTEM_SERVICES.get(name).getService(this);
    }

    @Override
    public String getProperty(String name) {
        return properties.getProperty(name);
    }

    private static class SystemService {
        public Object createService(ApplicationContext applicationContext){
            throw new RuntimeException();
        }
        public Object getService(ApplicationContext applicationContext) {
            return createService(applicationContext);
        }
    }
}
