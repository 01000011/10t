package com.ch.iot.framework;

import com.ch.iot.arm.Button;

import java.util.logging.Logger;

/**
 * Created by courtney harleston
 * 10/3/15
 */
public class IoTApplication extends ContextWrapper {
    private static final Logger LOGGER = Logger.getLogger(IoTApplication.class.getName());
    private SystemManager systemManager;
    private Button button;
    private String deviceId;
    public IoTApplication(){

    }

    public void onStart() throws Exception{

    }

    public final void attach(Context context) {
        setBaseContext(context);
        onInitialize();
    }

    protected void onInitialize() {

    }

    public final void start() throws Exception{
        deviceId = getProperty("deviceId");
        LOGGER.info("starting iot application device =====> " + deviceId);
        button = Button.read(Integer.parseInt(getProperty("button.pin")), new IoTButton(this));
        startServices();
        try {
            onStart();
            systemManager.success();
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            systemManager.failure();
        }
    }

    private void startServices() {
        systemManager = (SystemManager)getSystemService(SYSTEM_SERVICE);
    }

    public final void stop() {
        LOGGER.info("stopping iot application device =====> " + deviceId);
        button.release();
        off();
        onStop();
    }

    private void off() {
        try {
            systemManager.off();
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

    protected void onStop() {

    }

    private class IoTButton implements Button.ButtonAction {
        private IoTApplication ioTApplication;

        public IoTButton(IoTApplication ioTApplication) {
            this.ioTApplication = ioTApplication;
        }

        @Override
        public void onButtonPress() {
            LOGGER.info("onButtonPress");
            try {
                ((StatusManager) ioTApplication.getSystemService(Context.STATUS_SERVICE)).updateStatus();
            } catch (Exception e) {
                LOGGER.severe(e.getMessage());
            }
        }

        @Override
        public void onButtonRelease() {

        }

        @Override
        public long getButtonPollingInterval() {
            return 1;
        }

        @Override
        public void setButtonError(String e) {

        }
    }
}
