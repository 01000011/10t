# README #

This software is under the "Runs on my machine" architecture license.  It may work on your machine but I don’t really know so let me know when you find out.

### Code is for? ###

* Texas Instruments ARM boards

### How do I get set up? ###

* Install gradle to compile and create jars
     * "gradle build" to get individual jars
     * "gradle fatjar" to get jar with everything already packaged

### How do I execute jar? ###
* fatjar:  

```
#!sh
java -cp iot-application.all-1.0.jar com.ch.iot.application.ApplicationSystem

```