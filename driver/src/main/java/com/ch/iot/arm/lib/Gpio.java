package com.ch.iot.arm.lib;

import java.io.*;

/**
 * Created by courtney harleston
 * 10/1/15
 */
public final class Gpio {
    public static final String HIGH = "1";
    public static final String LOW = "0";
    public static final String IN = "in";
    public static final String OUT = "out";
    public static final String BASE_PATH = "/sys/class/gpio";

    public static void write(String gpioPin, String highLow) throws Exception {
        FileWriter fileWriter = new FileWriter(BASE_PATH + String.format("/gpio%s/value", gpioPin));
        fileWriter.write(highLow);
        fileWriter.flush();
        fileWriter.close();
    }

    public static String read(String gpioPin) throws Exception{
        char[] r = new char[1];
        FileReader fileReader = new FileReader(BASE_PATH + String.format("/gpio%s/value", gpioPin));
        if(fileReader.read(r) < 1){
            throw new Exception();
        }
        fileReader.close();
        return String.valueOf(r[0]);
    }

    public static boolean isAvailable(String gpioPin) {
        return new File(BASE_PATH + "/gpio" + gpioPin).exists();
    }

    public static boolean isInMode(String gpioPin, String mode) throws Exception {
        int data;
        StringBuilder stringBuilder = new StringBuilder();
        try(FileReader fileReader = new FileReader(BASE_PATH + String.format("/gpio%s/direction", gpioPin))){
            while((data = fileReader.read()) != -1) {
                stringBuilder.append((char) data);
            }
        }
        return mode.equals(stringBuilder.toString().trim());
    }

    public static void setup(String gpioPin, String mode) throws Exception {
        setupGpio(gpioPin);
        setGpioMode(gpioPin, mode);
    }

    public static void remove(String gpioPin) throws Exception {
        FileWriter fileWriter = new FileWriter(BASE_PATH + "/unexport");
        fileWriter.write(gpioPin);
        fileWriter.flush();
        fileWriter.close();
    }

    private static void setGpioMode(String gpioPin, String mode) throws Exception {
        FileWriter fileWriter =
                new FileWriter(BASE_PATH + String.format("/gpio%s/direction", gpioPin));
        fileWriter.write(mode);
        fileWriter.flush();
        fileWriter.close();
    }

    private static void setupGpio(String gpioPin) throws Exception{
        FileWriter fileWriter = new FileWriter(BASE_PATH + "/export");
        fileWriter.write(gpioPin);
        fileWriter.flush();
        fileWriter.close();
    }
}
