package com.ch.iot.arm;

import com.ch.iot.arm.lib.Gpio;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by courtney harleston
 * 10/1/15
 */

public class Button{
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private ButtonAction buttonAction;
    private String gpioPin;
    private volatile boolean running = true;
    private Button(int gpioPin, ButtonAction buttonAction) {
        this.gpioPin = String.valueOf(gpioPin);
        this.buttonAction = buttonAction;
        initialize();
    }

    private void initialize() {
        try {
            configure();
            startPolling();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private void startPolling() throws Exception {
        EXECUTOR_SERVICE.execute(() -> {
            while (running) {
                if (readPin() || sleep()) {
                    break;
                }
            }
        });
    }

    private boolean readPin() {
        try {
            if(Gpio.read(gpioPin).equals(Gpio.HIGH)) {
                buttonAction.onButtonPress();
                pauseUntilReleased();
                buttonAction.onButtonRelease();
            }
        } catch (Exception e) {
            return true;
        }
        return false;
    }

    private void pauseUntilReleased() throws Exception {
        while (running){
            if (Gpio.read(gpioPin).equals(Gpio.LOW)){
                break;
            }
        }
    }

    private boolean sleep() {
        try {
            Thread.sleep(buttonAction.getButtonPollingInterval());
        } catch (InterruptedException e) {
            buttonAction.setButtonError(e.getMessage());
            return true;
        }
        return false;
    }

    private void shutdownAndAwaitTermination() {
        EXECUTOR_SERVICE.shutdown();
        try {
            if (!EXECUTOR_SERVICE.awaitTermination(60, TimeUnit.SECONDS)) {
                EXECUTOR_SERVICE.shutdownNow();
                if (!EXECUTOR_SERVICE.awaitTermination(60, TimeUnit.SECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            EXECUTOR_SERVICE.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private void setup() throws Exception {
        Gpio.setup(gpioPin, Gpio.IN);
    }

    private void configure() throws Exception {
        if(Gpio.isAvailable(gpioPin)){
            checkMode();
        }else {
            setup();
        }
    }

    private void checkMode() throws Exception {
        if(!Gpio.isInMode(gpioPin, Gpio.IN)) {
            throw new RuntimeException("button is set to out");
        }
    }

    public void release() {
        running = false;
        shutdownAndAwaitTermination();
    }

    public static Button read(int gpioPin, ButtonAction buttonAction) {
        return ButtonHelper.INSTANCE(gpioPin, buttonAction);
    }

    public interface ButtonAction {
        void onButtonPress();
        void onButtonRelease();
        long getButtonPollingInterval();
        void setButtonError(String e);
    }

    private static class ButtonHelper {
        private static Button INSTANCE(int gpioPin, ButtonAction buttonAction){
            return new Button(gpioPin, buttonAction);
        }
    }
}