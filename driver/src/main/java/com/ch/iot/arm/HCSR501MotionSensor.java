package com.ch.iot.arm;

import com.ch.iot.arm.lib.Gpio;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by courtney harleston
 * 10/2/15
 */
public class HCSR501MotionSensor {
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    private final String gpioPin;
    private final SensorAction sensorAction;
    private volatile boolean running = true;

    public HCSR501MotionSensor(int gpioPin, SensorAction sensorAction){
        this.gpioPin = String.valueOf(gpioPin);
        this.sensorAction = sensorAction;
        initialize();
    }

    private void initialize() {
        try {
            configure();
            startPolling();
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private void configure() throws Exception {
        if(Gpio.isAvailable(gpioPin)){
            checkMode();
        }else {
            setup();
        }
    }

    private void checkMode() throws Exception {
        if(!Gpio.isInMode(gpioPin, Gpio.IN)) {
            throw new RuntimeException("sensor is set to out");
        }
    }

    private void setup() throws Exception {
        Gpio.setup(gpioPin, Gpio.IN);
    }

    public static HCSR501MotionSensor read(int gpioPin, SensorAction sensorAction){
        return HCSR501MotionSensorHelper.INSTANCE(gpioPin, sensorAction);
    }

    private void startPolling() throws Exception {
        EXECUTOR_SERVICE.execute(() -> {
            while (running) {
                if (readPin() || sleep()) {
                    break;
                }
            }
        });
    }

    private boolean readPin() {
        try {
            if(Gpio.read(gpioPin).equals(Gpio.HIGH)) {
                sensorAction.onMotionDetected();
                pauseUntilReleased();
                sensorAction.onMotionResetNormal();
            }
        } catch (Exception e) {
            return true;
        }
        return false;
    }

    private void pauseUntilReleased() throws Exception {
        while (running){
            if (Gpio.read(gpioPin).equals(Gpio.LOW)){
                break;
            }
        }
    }

    private boolean sleep() {
        try {
            Thread.sleep(sensorAction.getMotionPollingInterval());
        } catch (InterruptedException e) {
            sensorAction.setMotionError(e.getMessage());
            return true;
        }
        return false;
    }

    public void release() {
        running = false;
        shutdownAndAwaitTermination();
    }

    private void shutdownAndAwaitTermination() {
        EXECUTOR_SERVICE.shutdown();
        try {
            if (!EXECUTOR_SERVICE.awaitTermination(60, TimeUnit.SECONDS)) {
                EXECUTOR_SERVICE.shutdownNow();
                if (!EXECUTOR_SERVICE.awaitTermination(60, TimeUnit.SECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            EXECUTOR_SERVICE.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
    private static class HCSR501MotionSensorHelper {
        private static HCSR501MotionSensor INSTANCE(int gpioPin, SensorAction sensorAction){
           return new HCSR501MotionSensor(gpioPin, sensorAction);
        }
    }

    public interface SensorAction{

        void onMotionDetected();

        long getMotionPollingInterval();

        void setMotionError(String e);

        void onMotionResetNormal();
    }
}
