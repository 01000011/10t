package com.ch.iot.arm;

import com.ch.iot.arm.lib.Gpio;

/**
 * Created by courtney harleston
 * 10/1/15
 */
public class Light {
    private String gpioPin;

    private Light(int gpioPin){
        this.gpioPin = String.valueOf(gpioPin);
        initialize();
    }

    public void on() throws Exception {
        Gpio.write(gpioPin, Gpio.HIGH);
    }

    public void off() throws Exception {
        Gpio.write(gpioPin, Gpio.LOW);
    }

    public void release() throws Exception{
        Gpio.remove(gpioPin);
    }

    private void initialize() {
        validate();
        create();
        reset();
    }

    private void validate() {
        if (Gpio.isAvailable(gpioPin)){
            throw new RuntimeException("light already in use");
        }
    }

    private void reset() {
        try{
            Gpio.write(gpioPin, Gpio.LOW);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private void create() {
        try {
            Gpio.setup(gpioPin, Gpio.OUT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Light open(int gpioPin) {
        return LightHelper.INSTANCE(gpioPin);
    }

    private static class LightHelper {
        private static Light INSTANCE(int gpioPin) {
            return new Light(gpioPin);
        }
    }
}
