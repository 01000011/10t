package com.ch.iot.application;

import com.ch.iot.application.impl.IoTApplicationImpl;
import com.ch.iot.framework.Context;
import com.ch.iot.framework.IoTApplication;
import com.ch.iot.framework.ApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by courtney harleston
 * 10/1/15
 */
public class ApplicationSystem {
    private final static Logger LOGGER = Logger.getLogger(ApplicationSystem.class.getName());
    private static ExecutorService EXECUTOR_SERVICE = Executors.newSingleThreadExecutor();
    public static void main(String[] args) throws Exception {
        shutdownHook(startService());
    }

    private static IoTApplication startService() throws Exception {
        final IoTApplication ioTApplication = new IoTApplicationImpl();
        ioTApplication.attach(getContext());
        EXECUTOR_SERVICE.execute(() -> {
            try {
                ioTApplication.start();
            } catch (Exception e) {
                LOGGER.severe(e.getMessage());
                try {
                    ioTApplication.stop();
                    shutdownAndAwaitTermination();
                } catch (Exception e1) {
                    LOGGER.severe("could not stop application");
                    LOGGER.severe(e1.getMessage());
                }
            }
        });
        return ioTApplication;
    }

    private static Context getContext() throws Exception {
        return new ApplicationContext();
    }

    private static void shutdownHook(final IoTApplication ioTApplication) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    LOGGER.info("shutting down application");
                    ioTApplication.stop();
                    LOGGER.info("application shut down complete");
                    shutdownAndAwaitTermination();
                } catch (Exception e) {
                    LOGGER.severe("could not kill application:  " + e.getMessage());
                }
            }
        });
    }

    private static void shutdownAndAwaitTermination() {
        EXECUTOR_SERVICE.shutdown();
        try {
            if (!EXECUTOR_SERVICE.awaitTermination(60, TimeUnit.SECONDS)) {
                EXECUTOR_SERVICE.shutdownNow();
                if (!EXECUTOR_SERVICE.awaitTermination(60, TimeUnit.SECONDS)) {
                    LOGGER.severe("Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            LOGGER.severe(ie.getMessage());
            EXECUTOR_SERVICE.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}