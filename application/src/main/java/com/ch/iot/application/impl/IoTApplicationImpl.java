package com.ch.iot.application.impl;

import com.ch.iot.framework.IoTApplication;
import com.ch.iot.framework.LightManager;
import com.ch.iot.framework.StatusManager;

import java.util.logging.Logger;

/**
 * Created by courtney harleston
 * 10/3/15
 */
public class IoTApplicationImpl extends IoTApplication implements StatusManager.StatusChange {
    private static Logger LOGGER = Logger.getLogger(IoTApplicationImpl.class.getName());
    private LightManager lightManager;
    @Override
    public void onStart() throws Exception {
        lightManager = (LightManager)getSystemService(LIGHT_SERVICE);
        ((StatusManager)getSystemService(STATUS_SERVICE)).registerStatusChange(this);
    }

    private void toggleLight(int status) {
        if (status == 1) {
            turnOnLight();
        } else {
            turnOffLight();
        }
    }

    private void turnOffLight() {
        try {
            lightManager.off();
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private void turnOnLight() {
        try {
            lightManager.on();
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

    @Override
    public void setStatus(int status) {
        toggleLight(status);
    }
}
