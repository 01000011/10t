package com.ch.mobile;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class TnO extends Activity {
    public static final String TAG = "TnO";
    private TextView textView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tapnoccupy);
        textView = (TextView) findViewById(R.id.textView_status);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            String type = intent.getType();
            if ("text/plain".equals(type)) {
                new NdefReaderTask().execute((Tag)intent.getParcelableExtra(NfcAdapter.EXTRA_TAG));
            } else {
                Log.d(TAG, type + ":  not supported");
            }
        }
    }

    private class HttpRequestTask extends AsyncTask<String, String, String[]>{
        public static final String ONE = "1";
        public static final String TWO = "2";
        public static final String UPDATE = "UPDATE";
        public static final String COMPLETE = "COMPLETE";
        public static final String ERROR = "ERROR";

        @Override
        protected String[] doInBackground(String... uri) {
            try {
                if (ONE.equals(uri[0])) {
                    return new String[]{UPDATE, uri[1], uri[2], String.valueOf(getDeviceStatus(uri[1], uri[2]))};
                }
                if (TWO.equals(uri[0])) {
                    updateDeviceStatus(uri[1], uri[2], Integer.parseInt(uri[3]));
                    return new String[]{COMPLETE, uri[2], uri[3]};
                }
            } catch (Exception e) {
                return new String[]{ERROR, e.getMessage()};
            }
            return new String[0];
        }

        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            switch (result[0]){
                case UPDATE:
                    new HttpRequestTask().execute(TWO, result[1], result[2], result[3]);
                    break;
                case COMPLETE:
                    textView.setText((Integer.parseInt(result[2]) == 1) ? result[1] + " IS SET TO OCCUPIED" :
                            result[1] + " IS SET TO UNOCCUPIED");
                    break;
                case ERROR:
                    textView.setText(result[1]);
            }
        }

        private int getDeviceStatus(String url, String deviceId) throws Exception{
            URL u = new URL(url);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(u.openStream(), "UTF-8"));
            JSONArray jsonArray = new JSONArray(bufferedReader.readLine());
            bufferedReader.close();
            for (int i = 0; i < jsonArray.length(); ++i){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if (deviceId.equals(String.valueOf(jsonObject.get("deviceId")))){
                    return jsonObject.getBoolean("isOccupied") ? 1 : 0;
                }
            }
            return 0;
        }

        private void updateDeviceStatus(String url, String deviceId, int status) throws Exception {
            HttpURLConnection httpURLConnection = (HttpURLConnection)
                    new URL(String.format("%s?device=%s&occupied=%s", url, deviceId, status ^ 1)).openConnection();
            if (httpURLConnection.getResponseCode() != HttpsURLConnection.HTTP_OK) {
                throw new Exception("http status failed with " + httpURLConnection.getResponseCode());
            }
            httpURLConnection.disconnect();
        }
    }

    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {

        @Override
        protected String doInBackground(Tag... params) {
            Ndef ndef = Ndef.get(params[0]);
            if (ndef != null) {
                NdefMessage ndefMessage = ndef.getCachedNdefMessage();
                NdefRecord[] ndefRecords = ndefMessage.getRecords();
                for (NdefRecord ndefRecord : ndefRecords) {
                    if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(),
                            NdefRecord.RTD_TEXT)) {
                        try {
                            return readText(ndefRecord);
                        } catch (UnsupportedEncodingException e) {
                            Log.e(TAG, print(e));
                        }
                    }
                }
            }
            return null;
        }

        private String readText(NdefRecord ndefRecord) throws UnsupportedEncodingException {
            byte[] payload = ndefRecord.getPayload();
            int languageCodeLength = payload[0] & 63;
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1,
                    ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16");
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                String[] urlDeviceId = extractUrlDeviceId(result);
                new HttpRequestTask().execute(HttpRequestTask.ONE, urlDeviceId[0], urlDeviceId[1]);
            }else {
                Log.e(TAG, "Invalid tag information");
            }
        }
    }

    private String print(Exception e) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(os);
        e.printStackTrace(ps);
        try {
            return os.toString("UTF8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    private String[] extractUrlDeviceId(String result) {
        return result.split("\\|");
    }



    private void doPost(String url, String deviceId, int status) {
        try {
            URL u = new URL(String.format("%s?device=%s&occupied=%s", url, deviceId, status ^ 1));
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            String input = "{\"qty\":100,\"name\":\"iPad 4\"}";
            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();
            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed:  HTTP error code:  " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }
            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        URL u = new URL("http://iotdbsatl.herokuapp.com/api");
        BufferedReader reader = new BufferedReader(new InputStreamReader(u.openStream(), "UTF-8"));
        String json = reader.readLine();

        // Instantiate a JSON object from the request response
        JSONObject jsonObject = new JSONObject(json);
        int y=0;
    }

}
